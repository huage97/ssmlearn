package com.huage.mybatis.service;

import com.github.pagehelper.PageHelper;
import com.huage.mybatis.mapper.UserMapper;
import com.huage.mybatis.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserServiceTest {

    @Test
    public void testFindById() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = new User();
        user.setId(1);
        user.setUsername("zhangsan");
//        user.setPassword("50");
        User user1 = userMapper.findById(user);
        System.out.println(user1);

    }
    @Test
    public void testFindByIds() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(4);
        List<User> userList = userMapper.findByIds(list);
        System.out.println(userList);

    }
    @Test
    public void testMyHandler() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        User user = new User();
        user.setUsername("张三");
        user.setPassword("9999");
        user.setBirthday(new Date());
        userMapper.save(user);
        User user1 = userMapper.findById(user);
        System.out.println(user1.getBirthday());


    }
    @Test
    public void testPageHelper() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        PageHelper.startPage(2,3);
        List<User> userList = userMapper.findAll();
        for (User user : userList) {
            System.out.println(user);
        }


    }
}