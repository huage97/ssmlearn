package com.huage.mybatis.mapper;

import com.huage.mybatis.domain.User;

import java.util.List;

public interface UserMapper {
    public List<User> findAll();
    public User findById(User user);
    public void save(User user);
    public void set(User user);
    public void del(User user);
    public List<User> findByIds(List<Integer> list);
}
