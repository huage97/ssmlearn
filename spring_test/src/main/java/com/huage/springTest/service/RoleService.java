package com.huage.springTest.service;

import com.huage.springTest.domain.Role;

import java.util.List;

public interface RoleService {
    List<Role> findAll();


    void save(Role role);

}
