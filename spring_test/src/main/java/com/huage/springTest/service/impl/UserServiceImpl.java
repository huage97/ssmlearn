package com.huage.springTest.service.impl;

import com.huage.springTest.dao.RoleDao;
import com.huage.springTest.dao.UserDao;
import com.huage.springTest.domain.Role;
import com.huage.springTest.domain.User;
import com.huage.springTest.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {


    private UserDao userDao;
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    private RoleDao roleDao;
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public List<User> list() {
        List<User> userList = userDao.findAll();
        for (User user : userList){
            List<Role> roles = roleDao.findById(user.getId());
            user.setRoles(roles);
        }
        return userList;
    }

    @Override
    public void save(User user, Long[] roleIds) {
        Long userId = userDao.save(user);
        userDao.saveUserRoleRel(userId,roleIds);
    }
    @Override
    public void del(Long userId) {
        userDao.delUserRoleRel(userId);
        userDao.del(userId);
    }

}
