package com.huage.springTest.dao;

import com.huage.springTest.domain.User;

import java.util.List;

public interface UserDao {
    public  List<User> findAll();

    Long save(User user);

    void saveUserRoleRel(Long userId,Long[] roleIds);

    void delUserRoleRel(Long userId);

    void del(Long userId);
}
