package com.huage.springTest.dao;

import com.huage.springTest.domain.Role;

import java.util.List;

public interface RoleDao {
    List<Role> findAll();

    void save(Role role);

    List<Role> findById(long id);
}
