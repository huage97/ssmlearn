package com.huage.springTest.controller;

import com.huage.springTest.domain.Role;
import com.huage.springTest.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping("/list")
    public ModelAndView findAll(){
        ModelAndView modelAndView = new ModelAndView();
        List<Role> roleList = roleService.findAll();
        modelAndView.addObject("roleList",roleList);
        modelAndView.setViewName("role-list");
        System.out.println(roleList);
        return modelAndView;
    }
    @RequestMapping("/save")
    public String save(Role role){
        System.out.println(role.getRoleName()+","+role.getRoleDesc());
        roleService.save(role);

        return "redirect:/role/list";
    }
}
