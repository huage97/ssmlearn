package com.huage.anno;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component("myAspect")
@Aspect
public class MyAspect {
    @Before("execution(* com.huage.anno.*.*(..))")
    public void before(){
        System.out.println("前置增强...");
    }
    @AfterReturning("execution(* com.huage.anno.*.*(..))")
    public void afterRetuning(){
        System.out.println("后置增强...");
    }
}
