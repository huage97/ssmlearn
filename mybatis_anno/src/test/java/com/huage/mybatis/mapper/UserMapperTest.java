package com.huage.mybatis.mapper;

import com.huage.mybatis.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

public class UserMapperTest {

    private UserMapper userMapper;
    @Before
    public void Init() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        userMapper = sqlSession.getMapper(UserMapper.class);
    }
    @Test
    public void testSave() {

        User user = new User();
        user.setId(8);
        user.setUsername("张无忌");
        user.setPassword("zwj666");
        userMapper.save(user);

    }
    @Test
    public void testUpdate() {
        User user = new User();
        user.setId(8);
        user.setUsername("张无忌");
        user.setPassword("zwj667");
        userMapper.update(user);
    }
    @Test
    public void testDelete() {
        userMapper.delete(8);
    }
    @Test
    public void testFindById() {

        List<User> userList = userMapper.findById(8);
        for (User user : userList) {
            System.out.println(user);
        }
    }
    @Test
    public void testFindAll() {

        List<User> userList = userMapper.findAll();
        for (User user : userList) {
            System.out.println(user);
        }
    }

}