package com.huage.mybatis.mapper;

import com.huage.mybatis.domain.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserMapper {
    @Select("select * from user")
    public List<User> findAll();

    @Select("select * from user where id=#{id}")
    public List<User> findById(int id);

    @Insert("insert  into  user values (#{id},#{username},#{password})")
    public void save(User user);

    @Update(" update user set username=#{username},password=#{password} where id=#{id}")
    public void update(User user);

    @Delete("delete  from user where id=#{id}")
    public void delete(int id);

}
