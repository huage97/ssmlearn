package com.huage.domain;

import com.huage.config.SpringConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfiguration.class})
public class UserTest {

//    @Autowired
//    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Test
    public void testInsert() {
        jdbcTemplate.update("insert into user values(? ,?,?) ",null,"tomcat",24);
    }
    @Test
    public void testDelete(){
        jdbcTemplate.update("delete from user where name=?","tom");
    }
    @Test
    public void testUpdate(){
        jdbcTemplate.update("update  user set age=? where name=?",28,"tomcat");
    }
    @Test
    public void testQuery(){
        List<User> userList = jdbcTemplate.query("select * from user", new BeanPropertyRowMapper<User>(User.class));
        System.out.println(userList);
    }
    @Test
    public void testQueryOne(){
        User user = jdbcTemplate.queryForObject("select * from user where name=?", new BeanPropertyRowMapper<User>(User.class),"tomcat");
        System.out.println(user);
    }
    @Test
    public void testQueryCount(){
        Long count = jdbcTemplate.queryForObject("select count(*) from user", Long.class);
        System.out.println(count);
    }
}