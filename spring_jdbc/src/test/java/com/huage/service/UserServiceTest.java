package com.huage.service;

import com.huage.config.SpringConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=SpringConfiguration.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;
    @Test
    public void transfer() {
        userService.transfer("zhangsan","lisi",50);
//        userService.transfer("lisi","zhangsan",50);
    }
}