package com.huage.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@Import({JdbcConfiguration.class})
@ImportResource("classpath:applicationContext.xml")
@ComponentScan("com.huage")
public class SpringConfiguration {
}
