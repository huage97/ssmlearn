package com.huage.service.impl;

import com.huage.dao.UserDao;
import com.huage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.REQUIRED,timeout = 100,readOnly = false)
    public void transfer(String outMan, String inMan, int money) {
        userDao.out(outMan,money);
//        int n=1/0;
        userDao.in(inMan,money);
    }
}
