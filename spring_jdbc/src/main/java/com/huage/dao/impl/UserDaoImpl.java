package com.huage.dao.impl;

import com.huage.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl implements UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public void out(String outMan,int money) {
        jdbcTemplate.update("update user set age=age-? where name=?",money,outMan);
    }

    @Override
    public void in(String inMan,int money) {
        jdbcTemplate.update("update user set age=age+? where name=?",money,inMan);
    }
}
