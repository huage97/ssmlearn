package com.huage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user")
public class TargetController {

    @RequestMapping("/load")
    public ModelAndView save(ModelAndView modelAndView){
        System.out.println("服务器已收到");
        modelAndView.addObject("name","张三");
        modelAndView.setViewName("load");
        return  modelAndView;
    }

}
