package com.huage.listener;

import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;

public class WebServletContextUtils {
    public static ApplicationContext getApplicationContext(ServletContext servletContext){
        return (ApplicationContext) servletContext.getAttribute("app");
    }
}
