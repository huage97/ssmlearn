package com.huage.vo;

import com.huage.domain.User;

import java.util.ArrayList;

public class VO {
    private ArrayList<User> userList;

    public VO() {
    }

    public VO(ArrayList<User> userList) {
        this.userList = userList;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }

    @Override
    public String toString() {
        return "VO{" +
                "userList=" + userList +
                '}';
    }
}
