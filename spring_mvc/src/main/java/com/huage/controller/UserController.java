package com.huage.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.huage.domain.User;
import com.huage.vo.VO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

@Controller
@RequestMapping("/user")
public class UserController {

    //页面跳转
    @RequestMapping("/load")
    public String save(){
        System.out.println("Controller save running...");
        return "success";
    }

    @RequestMapping("/load1")
    public ModelAndView save1(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("username","张三");
        modelAndView.setViewName("hello");
        return modelAndView;
    }
    @RequestMapping("/load2")
    public ModelAndView save2(ModelAndView modelAndView){
        modelAndView.addObject("username","李四");
        modelAndView.setViewName("hello");
        return modelAndView;
    }
    @RequestMapping("/load3")
    public String save3(Model model){
        model.addAttribute("username","王五");
        return "hello";
    }
    //回写数据
    @RequestMapping("/out1")
    public void out1(HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=gbk");
        response.getWriter().write("服务器已响应");
    }
    @ResponseBody
    @RequestMapping("/out2")
    public String out2(){
//        return "服务器已响应消息体";
        return "Hello World";
    }
    @ResponseBody
    @RequestMapping("/out3")
    public String out3() throws JsonProcessingException {
        User user = new User();
        user.setName("张三");
        user.setAge(23);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(user);
        return json;
    }
    @ResponseBody
    @RequestMapping("/out4")
    public User out4() {
        User user = new User();
        user.setName("李四");
        user.setAge(24);

        return user;
    }

    //获得请求参数
    @ResponseBody
    @RequestMapping("/get1")
    public void get1(String username,int age) {
        System.out.println(username);
        System.out.println(age);
    }
    @ResponseBody
    @RequestMapping("/get2")
    public void get2(User user) {
        System.out.println(user);
    }
    @ResponseBody
    @RequestMapping("/get3")
    public void get3(String[] strs) {
        System.out.println(Arrays.asList(strs));
    }
    @ResponseBody
    @RequestMapping("/get4")
    public void get4(VO vo) {
        System.out.println(vo);
    }
    @ResponseBody
    @RequestMapping("/get5/{name}")
    public void get5(@PathVariable(value = "name",required = false) String username) {
        System.out.println(username);
    }
    @ResponseBody
    @RequestMapping("/get6")
    public void get6(Date date) {
        System.out.println(date);
    }

    //获取请求头
    @ResponseBody
    @RequestMapping("/getHeader")
    public void getHeader(@RequestHeader(value = "User-Agent",required = true) String userAgent) {
        System.out.println(userAgent);
    }
    @ResponseBody
    @RequestMapping("/getCookies")
    public void getCookies(@CookieValue(value = "JSESSIONID",required = true) String JSESSIONID) {
        System.out.println(JSESSIONID);
    }

    //文件上传
    @ResponseBody
    @RequestMapping( "/upload")
    public void upload(String username,MultipartFile uploadFile) throws IOException {
        System.out.println(username);
        String originalFilename = uploadFile.getOriginalFilename();
        System.out.println(originalFilename);
        uploadFile.transferTo(new File("D:\\document\\java\\CopyTest\\"+originalFilename));
        System.out.println("文件上传成功！");
    }
}
