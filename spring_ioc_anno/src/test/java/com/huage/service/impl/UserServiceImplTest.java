package com.huage.service.impl;

import com.huage.config.SpringConfiguration;
import com.huage.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("classpath:applicationContext.xml")
@ContextConfiguration(classes = {SpringConfiguration.class})
public class UserServiceImplTest {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private UserService userService;
    @Test
    public void testSave() throws SQLException {
        System.out.println("------");
        System.out.println(dataSource.getConnection());
        userService.save();
    }
}