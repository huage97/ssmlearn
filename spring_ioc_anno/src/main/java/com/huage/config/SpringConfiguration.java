package com.huage.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
//<context:component-scan base-package="com.huage"></context:component-scan>
@ComponentScan("com.huage")
//<import resource="applicationContext-mysql.xml"></import>
@Import({DataSourceConfiguration.class})
public class SpringConfiguration {

}
