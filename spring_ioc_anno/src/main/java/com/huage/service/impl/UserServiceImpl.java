package com.huage.service.impl;

import com.huage.dao.UserDao;
import com.huage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

//<bean id="userService" class="com.huage.service.impl.UserServiceImpl">
//<property name="userDao" ref="userDao"></property>
//</bean>
@Service("userService")
public class UserServiceImpl implements UserService {

    @Value("${jdbc.url}")
    private String url;

//    @Autowired
//    @Qualifier("userDao")
    @Resource(name = "userDao")
    private UserDao userDao;
//    public void setUserDao(UserDao userDao) {
//        this.userDao = userDao;
//    }

    @Override
    public void save() {
        System.out.println(url);
        userDao.save();
    }
}
