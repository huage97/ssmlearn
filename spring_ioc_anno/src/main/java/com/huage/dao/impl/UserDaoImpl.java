package com.huage.dao.impl;

import com.huage.dao.UserDao;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

//    <bean id="userDao" class="com.huage.dao.impl.UserDaoImpl"></bean>
//@Component
@Repository("userDao")
public class UserDaoImpl implements UserDao {
    @Override
    public void save() {
        System.out.println("save running...");
    }
}
