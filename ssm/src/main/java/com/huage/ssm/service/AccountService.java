package com.huage.ssm.service;

import com.huage.ssm.domain.Account;

import java.util.List;

public interface AccountService {

    void save(Account account);

    List<Account> findAll();
}
