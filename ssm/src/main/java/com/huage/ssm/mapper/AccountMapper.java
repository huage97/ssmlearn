package com.huage.ssm.mapper;

import com.huage.ssm.domain.Account;

import java.util.List;

public interface AccountMapper {

    void save(Account account);

    List<Account> findAll();
}
