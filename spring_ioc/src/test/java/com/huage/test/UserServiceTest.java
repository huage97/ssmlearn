package com.huage.test;

import com.huage.service.UserService;
import com.huage.service.impl.UserServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserServiceTest {
    @Test
    public void testSave(){
        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = (UserService) app.getBean("userService");
        userService.save();

    }
    @Test
    public void testNewService(){
        UserService userService = new UserServiceImpl();
        userService.save();
    }
}
