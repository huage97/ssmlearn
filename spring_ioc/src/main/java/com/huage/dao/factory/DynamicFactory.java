package com.huage.dao.factory;

import com.huage.dao.UserDao;
import com.huage.dao.impl.UserDaoImpl;

public class DynamicFactory {
    public UserDao getUserDao(){
        return new UserDaoImpl();
    }
}
