package com.huage.dao.impl;

import com.huage.dao.UserDao;



public class UserDaoImpl implements UserDao {
    public UserDaoImpl() {
        System.out.println("UserDao被创建...");
    }
    public void init(){
        System.out.println("init_method running...");
    }
    public void destroy(){
        System.out.println("destroy_method running...");
    }
    @Override
    public void save() {
        System.out.println("Save...");
    }

}
