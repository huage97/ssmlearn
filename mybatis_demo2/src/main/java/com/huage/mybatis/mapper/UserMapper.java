package com.huage.mybatis.mapper;

import com.huage.mybatis.domain.User;

import java.util.List;

public interface UserMapper {
    public List<User> findAll();
    public List<User> findAllRole();

}
