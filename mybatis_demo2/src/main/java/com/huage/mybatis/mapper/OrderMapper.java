package com.huage.mybatis.mapper;

import com.huage.mybatis.domain.Order;

import java.util.List;

public interface OrderMapper {
    public List<Order> findAll();
}
